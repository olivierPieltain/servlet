/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package msn.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import msn.controller.LogInBacking;
import msn.model.entity.MsnMember;
import msn.model.facade.ModelFacade;

@WebServlet(name = "FollowStatus", urlPatterns = {"/FollowStatus"})
public class FollowStatus extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @EJB
    private LogInBacking backing;

    @EJB
    private ModelFacade model;

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pseudo = request.getParameter("pseudo");
        boolean follow = request.getParameter("follow").equals("true");
        MsnMember other = model.findMember(pseudo);
        if (follow) {
            model.follow(getBacking().getCurrentMember(), other);
        } else {
            model.unfollow(getBacking().getCurrentMember(), other);
        }
        getServletContext().getRequestDispatcher("/Members").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    /**
     * @return the backing
     */
    public LogInBacking getBacking() {
        return backing;
    }

    /**
     * @param backing the backing to set
     */
    public void setBacking(LogInBacking backing) {
        this.backing = backing;
    }

}